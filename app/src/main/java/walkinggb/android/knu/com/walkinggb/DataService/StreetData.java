package walkinggb.android.knu.com.walkinggb.DataService;

/**
 * Created by Arron on 2015-07-25.
 */
public class StreetData {
    private String _id;
    private String region;
    private String main_street;
    private String sub_street;
    private String route;
    private double distance;
    private String lead_time;
    private String level;
    private String image_url;
    private String description;
    private String latLon;

    public String getImageURL() {
        return image_url;
    }

    public void setImageURL(String image_url) {
        this.image_url = image_url;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getMain_street() {
        return main_street;
    }

    public void setMain_street(String main_street) {
        this.main_street = main_street;
    }

    public String getSub_street() {
        return sub_street;
    }

    public void setSub_street(String sub_street) {
        this.sub_street = sub_street;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getLead_time() {
        return lead_time;
    }

    public void setLead_time(String lead_time) {
        this.lead_time = lead_time;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLatLon() {
        return latLon;
    }

    public void setLatLon(String latLon) {
        this.latLon = latLon;
    }
}
