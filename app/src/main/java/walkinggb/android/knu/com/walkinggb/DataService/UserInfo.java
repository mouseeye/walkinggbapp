package walkinggb.android.knu.com.walkinggb.DataService;

import java.io.Serializable;

/**
 * Created by Arron on 2015-08-23.
 */
public class UserInfo implements Serializable{
    private String name;
    private String gender;
    private int age;
    private int weight;
    private int registered;
    private int distance;

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }


    public int isRegistered() {
        return registered;
    }

    public void setRegistered(int registered) {
        this.registered = registered;
    }


    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
