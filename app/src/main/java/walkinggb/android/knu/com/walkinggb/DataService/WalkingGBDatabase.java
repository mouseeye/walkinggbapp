package walkinggb.android.knu.com.walkinggb.DataService;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import walkinggb.android.knu.com.walkinggb.Loading.StreetDBInitAsync;

/**
 * Created by Arron on 2015-07-22.
 */
public class WalkingGBDatabase {

    //singleton instance
    private static WalkingGBDatabase database;

    public static String databaseName = "walkinggb.db";
    public static String streetTableName = "street";
    public static String walkingInfoTableName = "walkingInfo";
    public static String userInfoTableName = "userInfo";
    public static int tableVersion = 1;

    private DatabaseHelper dbHelper;

    private SQLiteDatabase db;

    private Context context;

    private WalkingGBDatabase(Context context) {
        this.context = context;
    }

    public static WalkingGBDatabase getDatabase(Context context) {
        if (database == null) {
            database = new WalkingGBDatabase(context);
        }
        return database;
    }

    public boolean open() {
        if (dbHelper == null) {
            dbHelper = new DatabaseHelper(context);
            db = dbHelper.getWritableDatabase();
        }

        if (db == null) {
            return false;
        }
        return true;
    }

    public void close() {
        if (db == null) {
            return;
        }
        db.close();
        dbHelper.close();
        dbHelper = null;
    }

    public Cursor rawQuery(String sql, String[] paramas) {
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, paramas);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public void execSQL(String sql) {
        db.execSQL(sql);
    }

    private class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, databaseName, null, tableVersion);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            makeUserInfoTable(db);
            makeStreetTable(db);
            makeWalkingInfoTable(db);
        }

        private void makeStreetTable(SQLiteDatabase db) {
            String DROP_SQL = "DROP TABLE if exists " + streetTableName;
            String CREATE_SQL = "CREATE TABLE " + streetTableName +
                    "(_id INTEGER," +
                    "region TEXT," +
                    "main_street TEXT," +
                    "sub_street TEXT," +
                    "route TEXT," +
                    "distance TEXT," +
                    "lead_time TEXT," +
                    "level TEXT," +
                    "image_url TEXT," +
                    "latlon TEXT," +
                    "description TEXT)";

            try {
                db.execSQL(DROP_SQL);
                db.execSQL(CREATE_SQL);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void makeWalkingInfoTable(SQLiteDatabase db) {
            String DROP_SQL = "DROP TABLE if exists " + walkingInfoTableName;
            String CREATE_SQL = "CREATE TABLE " + walkingInfoTableName +
                    "(_id integer PRIMARY KEY autoincrement," +
                    "region TEXT," +
                    "main_street TEXT," +
                    "sub_street TEXT," +
                    "created_date TEXT," +
                    "start_time TEXT," +
                    "end_time TEXT," +
                    "taken_time TEXT," +
                    "distance TEXT," +
                    "calorie TEXT," +
                    "step_num TEXT)";
            try {
                db.execSQL(DROP_SQL);
                db.execSQL(CREATE_SQL);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void makeUserInfoTable(SQLiteDatabase db) {
            String DROP_SQL = "DROP TABLE if exists " + userInfoTableName;

            String CREATE_SQL = "CREATE TABLE " + userInfoTableName +
                    "(_id integer PRIMARY KEY, " +
                    "name text, " +
                    "gender text, " +
                    "age integer, " +
                    "registered integer, "+
                    "distance integer, "+
                    "weight integer)";

            String sql = "INSERT INTO " + WalkingGBDatabase.userInfoTableName + "(_id, weight, distance, registered) values (1, 65, 0, 0)";

            try {
                db.execSQL(DROP_SQL);
                db.execSQL(CREATE_SQL);
                db.execSQL(sql);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            if (oldVersion < 2) {
                //
            }
        }

        @Override
        public void onOpen(SQLiteDatabase db) {
            Log.d("TAG", "onOpen  호출");
        }
    }
}
