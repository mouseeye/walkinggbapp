package walkinggb.android.knu.com.walkinggb.Detail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.URL;

import walkinggb.android.knu.com.walkinggb.DataService.StreetData;
import walkinggb.android.knu.com.walkinggb.MainMap.MainMapActivity;
import walkinggb.android.knu.com.walkinggb.R;
import walkinggb.android.knu.com.walkinggb.Utils.ProcessData;

public class DetailActivity extends ActionBarActivity {
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private ImageView bgImageView;

    DetailViewAdapter detailViewAdapter;

    private String subTitle;
    private StreetData streetData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //내용 뷰
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        //툴바 뷰
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        bgImageView = (ImageView) findViewById(R.id.bgImageView);


        detailViewAdapter = new DetailViewAdapter(this);
        subTitle = getIntent().getStringExtra("sub_street");
        DetailViewDataAsyncTask dataAsyncTask = new DetailViewDataAsyncTask(this);
        dataAsyncTask.execute(subTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(detailViewAdapter);
    }

    public void showRouteImage(View v) {
        Intent i = new Intent(this, RouteMapActivity.class);
        i.putExtra("subTitle", subTitle);
        i.putExtra("latLon", streetData.getLatLon());
        startActivity(i);
    }

    public void showVenture(View v) {
        Intent i = new Intent(this, MainMapActivity.class);
        i.putExtra("region", streetData.getRegion());
        i.putExtra("mainTitle", streetData.getMain_street());
        i.putExtra("subTitle", subTitle);
        i.putExtra("latLon", streetData.getLatLon());
        startActivity(i);
    }

    public void setStreetData(StreetData streetData) {
        this.streetData = streetData;
        detailViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    class DetailViewAdapter extends RecyclerView.Adapter<DetailViewHolder> {

        Context context;
        Bitmap bitmap;
        Handler handler = new Handler();

        public DetailViewAdapter(Context context) {
            this.context = context;
        }

        @Override
        public DetailViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_recycler_item, viewGroup, false);
            return new DetailViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final DetailViewHolder detailViewHolder, int i) {
            String subStreet = streetData.getSub_street();
            detailViewHolder.setSubTitle(subStreet);
            collapsingToolbarLayout.setTitle(subStreet);
            detailViewHolder.setRegion(streetData.getRegion());
            detailViewHolder.setMainStreet(streetData.getMain_street());
            detailViewHolder.setRoute(streetData.getRoute());
            detailViewHolder.setDistance(streetData.getDistance() + "km");
            detailViewHolder.setLeadTime(streetData.getLead_time());
            detailViewHolder.setLevel(streetData.getLevel());
            detailViewHolder.setDescription(streetData.getDescription());
            new Thread(new Runnable() {
                @Override
                public void run() {
                    bitmap = ProcessData.loadImageFromServer(streetData.getImageURL());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            detailViewHolder.setBgImageView1(bitmap);
                        }
                    });
                }
            }).start();
        }

        @Override
        public int getItemCount() {
            if (streetData == null) return 0;
            return 1;
        }
    }

    class DetailViewHolder extends RecyclerView.ViewHolder {

        TextView subTitle;
        TextView region;
        TextView mainStreet;
        TextView route;
        TextView distance;
        TextView leadTime;
        TextView level;
        TextView description;
        ImageView bgImageView1;

        public DetailViewHolder(View itemView) {
            super(itemView);
            subTitle = (TextView) itemView.findViewById(R.id.sub_title);
            region = (TextView) itemView.findViewById(R.id.region);
            mainStreet = (TextView) itemView.findViewById(R.id.main_street);
            route = (TextView) itemView.findViewById(R.id.route);
            distance = (TextView) itemView.findViewById(R.id.distance);
            leadTime = (TextView) itemView.findViewById(R.id.lead_time);
            level = (TextView) itemView.findViewById(R.id.level);
            description = (TextView) itemView.findViewById(R.id.description);
            bgImageView1 = bgImageView;
        }


        public void setSubTitle(String subTitle) {
            this.subTitle.setText(subTitle);
        }

        public void setRegion(String region) {
            this.region.setText(region);
        }

        public void setMainStreet(String mainStreet) {
            this.mainStreet.setText(mainStreet);
        }

        public void setRoute(String route) {
            this.route.setText(route);
        }

        public void setDistance(String distance) {
            this.distance.setText(distance);
        }

        public void setLeadTime(String leadTime) {
            this.leadTime.setText(leadTime);
        }

        public void setLevel(String level) {
            this.level.setText(level);
        }

        public void setDescription(String description) {
            this.description.setText(description);
        }

        public void setBgImageView1(Bitmap bgImageView1) {
            this.bgImageView1.setImageBitmap(bgImageView1);
        }
    }
}
