package walkinggb.android.knu.com.walkinggb.Detail;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import walkinggb.android.knu.com.walkinggb.DataService.StreetData;
import walkinggb.android.knu.com.walkinggb.DataService.WalkingGBDatabase;

/**
 * Created by Arron on 2015-07-30.
 */
public class DetailViewDataAsyncTask extends AsyncTask<String, StreetData, StreetData>{

    private Context context;
    private ProgressDialog dialog;
    private WalkingGBDatabase db;
    public DetailViewDataAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        db = WalkingGBDatabase.getDatabase(context);
        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setTitle("Data Loading...");
        dialog.show();
    }

    @Override
    protected StreetData doInBackground(String[] params) {
        db.open();
        String SQL = "SELECT * FROM " + WalkingGBDatabase.streetTableName + " WHERE sub_street = ?";
        Cursor cursor = db.rawQuery(SQL, new String[]{params[0]});

        if(cursor == null){
            return null;
        }

        StreetData streetData = parsingData(cursor);
        return streetData;
    }

    private StreetData parsingData(Cursor cursor) {
        StreetData streetData = new StreetData();

        while(cursor.moveToNext()){
            String region = cursor.getString(1);
            String main_street = cursor.getString(2);
            String sub_street = cursor.getString(3);
            String route = cursor.getString(4);
            String distance = cursor.getString(5);
            String lead_time = cursor.getString(6);
            String level = cursor.getString(7);
            String image_url = cursor.getString(8);
            String latLon = cursor.getString(9);
            String description = cursor.getString(10);


            streetData.setRegion(region);
            streetData.setMain_street(main_street);
            streetData.setSub_street(sub_street);
            streetData.setRoute(route);
            streetData.setDistance(Double.parseDouble(distance));
            streetData.setLead_time(lead_time);
            streetData.setLevel(level);
            streetData.setImageURL(image_url);
            streetData.setLatLon(latLon);
            streetData.setDescription(description);
        }
        return streetData;
    }

    @Override
    protected void onPostExecute(StreetData streetData) {
        super.onPostExecute(streetData);

        DetailActivity activity = (DetailActivity)context;
        activity.setStreetData(streetData);

        db.close();
        dialog.dismiss();
    }
}
