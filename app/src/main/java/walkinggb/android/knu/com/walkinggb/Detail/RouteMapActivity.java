package walkinggb.android.knu.com.walkinggb.Detail;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

import walkinggb.android.knu.com.walkinggb.R;
import walkinggb.android.knu.com.walkinggb.Utils.ProcessData;

/**
 * Created by Arron on 2015-07-31.
 */
public class RouteMapActivity extends AppCompatActivity {
    String subTitle;
    GoogleMap googleMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.route_map);

        Intent intent = getIntent();

        //액션바 세팅
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        subTitle = intent.getStringExtra("subTitle");
        actionBar.setTitle(subTitle);

        //맵 세팅
        String latLon = intent.getStringExtra("latLon");

        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        googleMap = supportMapFragment.getMap();

        ArrayList<LatLng> latLngs = ProcessData.requestRouteData(latLon);

        setGoogleMap(latLngs);
    }

    private void setGoogleMap(ArrayList<LatLng> latLngs) {

        PolylineOptions polylineOptions = new PolylineOptions();
        MarkerOptions startMarKerOptions = new MarkerOptions();
        MarkerOptions finishMarKerOptions = new MarkerOptions();

        polylineOptions.color(Color.RED);
        polylineOptions.width(15);
        polylineOptions.addAll(latLngs);

        startMarKerOptions.title("출발점");
        startMarKerOptions.position(latLngs.get(0));

        finishMarKerOptions.title("도착점");
        finishMarKerOptions.position(latLngs.get(latLngs.size() - 1));

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLngs.get(latLngs.size() / 2), 12));
        googleMap.addPolyline(polylineOptions);
        googleMap.addMarker(startMarKerOptions);
        googleMap.addMarker(finishMarKerOptions);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }
}
