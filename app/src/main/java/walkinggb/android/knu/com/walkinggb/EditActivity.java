package walkinggb.android.knu.com.walkinggb;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import walkinggb.android.knu.com.walkinggb.Loading.EditMyInfoFragment;
import walkinggb.android.knu.com.walkinggb.Utils.ProcessData;

public class EditActivity extends AppCompatActivity implements EditMyInfoFragment.OnEditButtonListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");
        actionBar.setHomeAsUpIndicator(R.drawable.abc_ic_clear_mtrl_alpha);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        EditMyInfoFragment fragment = new EditMyInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("USER_INFO", ProcessData.loadUserInfo(this));
        fragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, fragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onEditButtonClicked() {
        setResult(RESULT_OK, null);
        finish();
    }
}
