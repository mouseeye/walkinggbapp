package walkinggb.android.knu.com.walkinggb.Loading;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import walkinggb.android.knu.com.walkinggb.DataService.UserInfo;
import walkinggb.android.knu.com.walkinggb.DataService.WalkingGBDatabase;
import walkinggb.android.knu.com.walkinggb.Main.MainActivity;
import walkinggb.android.knu.com.walkinggb.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditMyInfoFragment extends Fragment {

    OnEditButtonListener mCallback;

    UserInfo userInfo;
    AppCompatEditText nameEditText;
    AppCompatEditText genderEditText;
    AppCompatEditText ageEditText;
    AppCompatEditText weightEditText;
    private static EditMyInfoFragment fragment;


    public static EditMyInfoFragment newInstance() {
        if (fragment == null) {
            fragment = new EditMyInfoFragment();
        }
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try{
            mCallback = (OnEditButtonListener)activity;
        }catch (Exception e){
            throw new ClassCastException("Exception : " + e);
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            userInfo = (UserInfo) bundle.getSerializable("USER_INFO");
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_edit_my_info, container, false);

        Button btn = (Button) rootView.findViewById(R.id.registerBtn);
        nameEditText = (AppCompatEditText) rootView.findViewById(R.id.name);
        genderEditText = (AppCompatEditText) rootView.findViewById(R.id.gender);
        ageEditText = (AppCompatEditText) rootView.findViewById(R.id.age);
        weightEditText = (AppCompatEditText) rootView.findViewById(R.id.weight);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameEditText.getText().toString();
                String gender = genderEditText.getText().toString();
                String age = ageEditText.getText().toString();
                String weight = weightEditText.getText().toString();
                Log.d("TAG", genderEditText.getText().toString());

                if (!name.equals("") && !gender.equals("") && !age.equals("") && !weight.equals("")) {
                    String sql = "UPDATE " + WalkingGBDatabase.userInfoTableName + " SET name = " + "'" + name + "'" + ", " +
                            "gender = " + "'" + gender + "'" + ", " +
                            "age = " + "'" + age + "'" + ", " +
                            "weight = " + "'" + weight + "'" + ", " +
                            "registered = " + "'" + 1 + "'" + " where _id =  1";
                    WalkingGBDatabase database = WalkingGBDatabase.getDatabase(getActivity());
                    database.open();
                    database.execSQL(sql);

                    mCallback.onEditButtonClicked();
                } else {
                    new AlertDialog.Builder(getActivity())
                            .setMessage("모든 정보를 입력해 주세요.")
                            .setTitle("데이터 입력 오류")
                            .show();
                }
            }
        });
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (userInfo != null) {
            nameEditText.setText(userInfo.getName());
            genderEditText.setText(userInfo.getGender());
            ageEditText.setText(userInfo.getAge()+"");
            weightEditText.setText(userInfo.getWeight()+"");
        }
    }

    public interface OnEditButtonListener{
        void onEditButtonClicked();
    }
}
