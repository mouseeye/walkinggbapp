package walkinggb.android.knu.com.walkinggb.Loading;

import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import walkinggb.android.knu.com.walkinggb.DataService.WalkingGBDatabase;
import walkinggb.android.knu.com.walkinggb.Main.MainActivity;
import walkinggb.android.knu.com.walkinggb.R;

public class LoadingActivity extends AppCompatActivity implements EditMyInfoFragment.OnEditButtonListener {
    WalkingGBDatabase db;
    LoadingFragment loadingFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        loadingFragment = LoadingFragment.newInstance();
        getSupportFragmentManager().beginTransaction().add(R.id.container_loading, loadingFragment).commit();
    }

    @Override
    protected void onResume() {
        db = WalkingGBDatabase.getDatabase(this);
        db.open();
        Handler hd = new Handler();
        if(isMyInfoRegistered()==0){
            StreetDBInitAsync streetDbInitAsync = new StreetDBInitAsync(db, this);
            streetDbInitAsync.execute();
        }else {
            hd.postDelayed(new Runnable() {
                @Override
                public void run() {
                    callMainActivity();
                }
            }, 1000);
        }
        super.onResume();
    }

    private int isMyInfoRegistered(){
        String sql = "SELECT registered FROM "+WalkingGBDatabase.userInfoTableName + " WHERE _id = ?";
        Cursor cursor = db.rawQuery(sql, new String[]{"1"});
        cursor.moveToNext();
        int result = cursor.getInt(0);
        return result;
    }

    @Override
    public void onEditButtonClicked() {
        callMainActivity();
    }

    private void callMainActivity(){
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }
}
