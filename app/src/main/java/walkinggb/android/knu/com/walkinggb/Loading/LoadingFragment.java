package walkinggb.android.knu.com.walkinggb.Loading;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import walkinggb.android.knu.com.walkinggb.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoadingFragment extends Fragment {

    private static LoadingFragment fragment;

    public static LoadingFragment newInstance(){
        if(fragment == null){
            fragment = new LoadingFragment();
        }
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_loading, container, false);
        ImageView loadingImage = (ImageView) rootView.findViewById(R.id.loadingImage);
        Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.progress);
        anim.setDuration(2000);
        loadingImage.startAnimation(anim);
        return rootView;
    }

}
