package walkinggb.android.knu.com.walkinggb.Loading;

import android.os.AsyncTask;
import android.util.Log;

import com.mongodb.BasicDBList;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import walkinggb.android.knu.com.walkinggb.DataService.StreetData;
import walkinggb.android.knu.com.walkinggb.DataService.WalkingGBDatabase;
import walkinggb.android.knu.com.walkinggb.R;

/**
 * Created by Arron on 2015-07-26.
 */
public class StreetDBInitAsync extends AsyncTask {

    private LoadingActivity activity;
    private WalkingGBDatabase database;

    public StreetDBInitAsync(WalkingGBDatabase database, LoadingActivity activity) {
        this.database = database;
        this.activity = activity;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        requestStreetData();
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        activity.getSupportFragmentManager().beginTransaction().replace(R.id.container_loading, EditMyInfoFragment.newInstance()).commit();
        database.close();
        database = null;
    }

    private void requestStreetData() {
        String returnedData = null;
        try {
                String address = "https://api.mongolab.com/api/1/databases/mouseeyedb/collections/street?apiKey=suLG07FIWkxtmiHcN-MTsuBYBUCn4yhh";
                URL url = new URL(address);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");
                int resCode = conn.getResponseCode();
                Log.d("TAG", resCode+"");
                if (resCode == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    returnedData = br.readLine();
                    initializeStreetTable(returnedData);
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeStreetTable(String returnedData) {
        String mongoarray = "{ artificial_basicdb_list: " + returnedData + "}";
        DBObject dbObject = (DBObject) JSON.parse(mongoarray);
        BasicDBList contacts = (BasicDBList) dbObject.get("artificial_basicdb_list");
        StreetData streetData = new StreetData();
        for (Object obj : contacts) {
            DBObject streetObj = (DBObject) obj;
            streetData.set_id(streetObj.get("_id").toString());
            streetData.setRegion(streetObj.get("region").toString());
            streetData.setMain_street(streetObj.get("main_street").toString());
            streetData.setSub_street(streetObj.get("sub_street").toString());
            streetData.setRoute(streetObj.get("route").toString());
            streetData.setDistance(Double.valueOf((String) streetObj.get("distance")));
            streetData.setLead_time(streetObj.get("lead_time").toString());
            streetData.setLevel(streetObj.get("level").toString());
            streetData.setImageURL(streetObj.get("image_url").toString());
            streetData.setLatLon(streetObj.get("latlon").toString());
            streetData.setDescription(streetObj.get("description").toString());
            insertDataIntoStreetTable(streetData);
        }
    }

    private void insertDataIntoStreetTable(StreetData streetData) {
        String init_SQL = "INSERT INTO " + WalkingGBDatabase.streetTableName + "(_id, region, main_street, sub_street, route, distance, lead_time, level, image_url, latlon, description) values (" +
                "'" + streetData.get_id() + "'," +
                "'" + streetData.getRegion() + "'," +
                "'" + streetData.getMain_street() + "'," +
                "'" + streetData.getSub_street() + "'," +
                "'" + streetData.getRoute() + "'," +
                streetData.getDistance() + "," +
                "'" + streetData.getLead_time() + "'," +
                "'" + streetData.getLevel() + "'," +
                "'" + streetData.getImageURL() + "'," +
                "'" + streetData.getLatLon() + "'," +
                "'" + streetData.getDescription() + "')";
        database.execSQL(init_SQL);
    }
}
