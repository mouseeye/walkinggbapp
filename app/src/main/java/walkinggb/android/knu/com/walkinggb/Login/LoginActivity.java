package walkinggb.android.knu.com.walkinggb.Login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;

import walkinggb.android.knu.com.walkinggb.Main.MainActivity;
import walkinggb.android.knu.com.walkinggb.Posting.PostingActivity;
import walkinggb.android.knu.com.walkinggb.R;

public class LoginActivity extends AppCompatActivity {

    CallbackManager callbackManager;
    AccessToken accessToken;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);

        checkLogined();
        requestFBLoginView();
    }

    private void checkLogined() {
        accessToken = AccessToken.getCurrentAccessToken();
        if(accessToken == null){
            return;
        }
        requestFBLoginView();
    }

    private void requestFBLoginView(){
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Intent intent = new Intent(LoginActivity.this, PostingActivity.class);
                intent.putExtra("walkingInfo", getIntent().getSerializableExtra("walkingInfo"));
                intent.putExtra("recordedLocation", getIntent().getSerializableExtra("recordedLocation"));
                startActivity(intent);
                LoginActivity.this.finish();
            }

            @Override
            public void onCancel() {
                Log.d("TAG", "자동 로그인 실패");
                LoginActivity.this.finish();
            }

            @Override
            public void onError(FacebookException e) {
                Log.d("TAG", "자동 로그인 실패  " + e.toString());
                LoginActivity.this.finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
