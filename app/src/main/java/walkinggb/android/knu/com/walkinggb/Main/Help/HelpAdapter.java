package walkinggb.android.knu.com.walkinggb.Main.Help;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import walkinggb.android.knu.com.walkinggb.R;

/**
 * Created by Arron on 2015-09-29.
 */
public class HelpAdapter extends PagerAdapter {
    int[] helpArr = {R.drawable.help3, R.drawable.help4, R.drawable.help5, R.drawable.help6, R.drawable.help7, R.drawable.help8, R.drawable.help9};
    LayoutInflater inflater;

    public HelpAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return helpArr.length;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        // TODO Auto-generated method stub

        View view = null;

        view = inflater.inflate(R.layout.help_view, null);

        ImageView img = (ImageView) view.findViewById(R.id.help_view);

        img.setImageResource(helpArr[position]);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub

        container.removeView((View) object);

    }

    @Override
    public boolean isViewFromObject(View v, Object obj) {
        // TODO Auto-generated method stub
        return v == obj;
    }
}
