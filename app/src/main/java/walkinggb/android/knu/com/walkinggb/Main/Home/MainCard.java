package walkinggb.android.knu.com.walkinggb.Main.Home;

/**
 * Created by Arron on 2015-07-19.
 */
public class MainCard {

    private String mainTitle;
    private String subTitle;
    private int imageId;


    public String getMainTitle() {
        return mainTitle;
    }

    public void setMainTitle(String mainTitle) {
        this.mainTitle = mainTitle;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImage(int imageId) {
        this.imageId = imageId;
    }

}
