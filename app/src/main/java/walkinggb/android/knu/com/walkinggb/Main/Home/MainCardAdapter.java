package walkinggb.android.knu.com.walkinggb.Main.Home;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by Arron on 2015-07-19.
 */
public class MainCardAdapter extends BaseAdapter{

    ArrayList<MainCard> mainCards = new ArrayList<>();
    Context context;
    public MainCardAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return mainCards.size();
    }

    @Override
    public Object getItem(int position) {
        return mainCards.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       MainCardView mainCardView;

        if(convertView == null){
            mainCardView = new MainCardView(context);
        }else{
            mainCardView = (MainCardView) convertView;
        }

        MainCard mainCard = mainCards.get(position);

        mainCardView.setBackgroundImage(mainCard.getImageId());
        mainCardView.setTitles(mainCard.getMainTitle(), mainCard.getSubTitle());

        return mainCardView;
    }


    public void setMainCards(ArrayList mainCards) {
        this.mainCards = mainCards;
    }
}
