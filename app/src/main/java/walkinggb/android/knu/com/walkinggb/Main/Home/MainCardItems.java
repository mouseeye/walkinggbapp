package walkinggb.android.knu.com.walkinggb.Main.Home;

import walkinggb.android.knu.com.walkinggb.R;

/**
 * Created by Arron on 2015-07-19.
 */
public class MainCardItems {
    public static final int KYUNGJU_IMAGE = R.drawable.kyungju_main;
    public static final int MOONKYUNG_IMAGE = R.drawable.moonkyung_main;
    public static final int ANDONG_IMAGE = R.drawable.andong_main;
    public static final int ULLEUNGDO_IMAGE = R.drawable.ulleungdo_main;
    public static final int CHUNGSONG_IMAGE = R.drawable.chungsong_main;

    public static final String KYUNGJU_MAINTITLE = "경북 경주시";
    public static final String MOONKYUNG_MAINTITLE = "경북 문경시";
    public static final String ANDONG_MAINTITLE = "경북 안동시";
    public static final String ULLEUNGDO_MAINTITLE = "경북 울릉도";
    public static final String CHUNGSONG_MAINTITLE = "경북 청송군";

    public static final String KYUNGJU_SUBTITLE = "감포깍지길";
    public static final String MOONKYUNG_SUBTITLE = "문경새재길";
    public static final String ANDONG_SUBTITLE = "유교 문화길";
    public static final String ULLEUNGDO_SUBTITLE = "울릉 둘레길";
    public static final String CHUNGSONG_SUBTITLE = "외씨버선길";

    public static int[] MAINCARD_IMAGES = {MainCardItems.KYUNGJU_IMAGE, MainCardItems.MOONKYUNG_IMAGE, MainCardItems.ANDONG_IMAGE, MainCardItems.ULLEUNGDO_IMAGE, MainCardItems.CHUNGSONG_IMAGE};

    public static String[] MAINCARD_MAINTITLE = {MainCardItems.KYUNGJU_MAINTITLE, MainCardItems.MOONKYUNG_MAINTITLE, MainCardItems.ANDONG_MAINTITLE, MainCardItems.ULLEUNGDO_MAINTITLE, MainCardItems.CHUNGSONG_MAINTITLE};

    public static String[] MAINCARD_SUBTITLE = {KYUNGJU_SUBTITLE, MOONKYUNG_SUBTITLE, ANDONG_SUBTITLE, ULLEUNGDO_SUBTITLE, CHUNGSONG_SUBTITLE};
}
