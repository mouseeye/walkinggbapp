package walkinggb.android.knu.com.walkinggb.Main.Home;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import walkinggb.android.knu.com.walkinggb.R;

/**
 * Created by Arron on 2015-07-19.
 */
public class MainCardView extends RelativeLayout {

    private Context context;
    private TextView mainTitle;
    private TextView subTitle;
    private ImageView backgroundImage;

    public MainCardView(Context context) {
        super(context);
        init(context);
    }

    public MainCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_card_main, this, true);
        mainTitle = (TextView) view.findViewById(R.id.mainTitle);
        subTitle = (TextView) view.findViewById(R.id.subTitle);
        backgroundImage = (ImageView) view.findViewById(R.id.backgroundImage);
        backgroundImage.setFocusable(false);
    }

    public void setTitles(String mainTitle, String subTitle) {

        if (mainTitle != null && subTitle != null) {
            this.mainTitle.setText(mainTitle);
            this.subTitle.setText(subTitle);
        }

    }

    public void setBackgroundImage(int imageId) {
        BitmapFactory.Options bOptions = new BitmapFactory.Options();
        bOptions.inJustDecodeBounds = false;
      //  bOptions.inSampleSize = 4;
        bOptions.inPurgeable = true;
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), imageId, bOptions);
        backgroundImage.setImageBitmap(bitmap);
    }
}
