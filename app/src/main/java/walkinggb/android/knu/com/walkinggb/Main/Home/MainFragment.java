package walkinggb.android.knu.com.walkinggb.Main.Home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import walkinggb.android.knu.com.walkinggb.Main.MainActivity;
import walkinggb.android.knu.com.walkinggb.R;
import walkinggb.android.knu.com.walkinggb.Sub.SubActivity;

/**
 * Created by PLUUSYSTEM-NEW on 2015-05-31.
 */
public class MainFragment extends Fragment {

    ListView cardsListView;
    Handler handler = new Handler();
    Context context;
    private static MainFragment fragment;

    public static MainFragment newInstance() {

        if (fragment == null) {
            fragment = new MainFragment();
        }
        return fragment;
    }

    public MainFragment() {
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        cardsListView = (ListView) rootView.findViewById(R.id.cardsListView);
        cardsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String main_street = MainCardItems.MAINCARD_SUBTITLE[position];

                Intent intent = new Intent(context, SubActivity.class);
                intent.putExtra("main_street", main_street);
                startActivity(intent);

                MainActivity ddd = (MainActivity) context;
                ddd.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        });


        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadMainCard();
    }

    private void loadMainCard() {
        MainCardAdapter mainCardAdapter = new MainCardAdapter(context);
        SettingMainCardsThread settingMainCardsThread = new SettingMainCardsThread(mainCardAdapter, handler);
        settingMainCardsThread.start();
        cardsListView.setAdapter(mainCardAdapter);
    }

}
