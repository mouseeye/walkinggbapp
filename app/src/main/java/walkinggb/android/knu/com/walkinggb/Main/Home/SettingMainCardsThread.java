package walkinggb.android.knu.com.walkinggb.Main.Home;

import android.os.Handler;

import java.util.ArrayList;

/**
 * Created by Arron on 2015-07-24.
 */
public class SettingMainCardsThread extends Thread {

    MainCardAdapter mainCardAdapter;
    ArrayList<MainCard> mainCards;
    Handler handler;

    public SettingMainCardsThread(MainCardAdapter mainCardAdapter, Handler handler) {
        this.mainCardAdapter = mainCardAdapter;
        this.handler = handler;
    }

    public void run() {
        mainCardAdapter.setMainCards(getCardItems());
        handler.post(new Runnable() {
            @Override
            public void run() {
                mainCardAdapter.notifyDataSetChanged();
            }
        });
    }


    private ArrayList getCardItems() {
        ArrayList mainCards = new ArrayList<>();

        for (int i = 0; i < MainCardItems.MAINCARD_MAINTITLE.length; i++) {
            MainCard mainCard = new MainCard();
            mainCard.setImage(getMainCardImage(i));
            mainCard.setMainTitle(getMainCardMainTitle(i));
            mainCard.setSubTitle(getMainCardSubTitle(i));
            mainCards.add(mainCard);
        }
        return mainCards;
    }

    private String getMainCardSubTitle(int index) {
        String subTitle = "#"+MainCardItems.MAINCARD_SUBTITLE[index];
        return subTitle;
    }

    private String getMainCardMainTitle(int index) {
        String mainTitle = "#"+MainCardItems.MAINCARD_MAINTITLE[index];
        return mainTitle;
    }

    private int getMainCardImage(int index) {
        int imageId = MainCardItems.MAINCARD_IMAGES[index];
        return imageId;
    }

}
