package walkinggb.android.knu.com.walkinggb.Main;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;


import walkinggb.android.knu.com.walkinggb.EditActivity;
import walkinggb.android.knu.com.walkinggb.Main.Help.HelpActivity;
import walkinggb.android.knu.com.walkinggb.Main.Home.MainFragment;
import walkinggb.android.knu.com.walkinggb.Main.MyInfo.MyInfoFragment;
import walkinggb.android.knu.com.walkinggb.Main.MyRoute.MyRouteFragment;
import walkinggb.android.knu.com.walkinggb.R;
import walkinggb.android.knu.com.walkinggb.Utils.ProcessData;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawerLayout;
    Toolbar toolBar;
    NavigationView navigationView;
    private ActionBarDrawerToggle mDrawerToggle;

    final int REQ_EDIT = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toolBar = (Toolbar) findViewById(R.id.main_tool_bar);
        navigationView = (NavigationView) findViewById(R.id.main_drawer_view);

        setSupportActionBar(toolBar);

        mDrawerToggle
                = new ActionBarDrawerToggle(this, drawerLayout, toolBar,
                R.string.app_name, R.string.app_name);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerListener(mDrawerToggle);

        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();

        MainFragment.newInstance().setContext(this);

        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction()
                .replace(R.id.main_frame, MainFragment.newInstance())
                .commit();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();

        Fragment fragment = null;
        switch (id) {
            case R.id.navi_group1_home:
                fragment = MainFragment.newInstance();
                break;
            case R.id.navi_group1_myinfo:
                fragment = MyInfoFragment.newInstance();
                ((MyInfoFragment) fragment).setUserInfo(ProcessData.loadUserInfo(this));
                break;
            case R.id.navi_group1_myroute:
                fragment = MyRouteFragment.newInstance();
                break;
            case R.id.navi_group1_help:
                fragment = MainFragment.newInstance();
                Intent intent = new Intent(MainActivity.this, HelpActivity.class);
                startActivity(intent);
                break;
        }

        if (fragment != null) {
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction()
                    .replace(R.id.main_frame, fragment)
                    .commit();

            drawerLayout.closeDrawers();
            menuItem.setChecked(true);
        }
        return true;
    }

    public void onEditInfoButton(View view) {
        Intent intent = new Intent(this, EditActivity.class);
        startActivityForResult(intent, REQ_EDIT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_EDIT) {
            if (resultCode == RESULT_OK) {
                MyInfoFragment fragment = MyInfoFragment.newInstance();
                fragment.setUserInfo(ProcessData.loadUserInfo(this));
                fragment.notifyDataChanged();
            }
        }
    }
}
