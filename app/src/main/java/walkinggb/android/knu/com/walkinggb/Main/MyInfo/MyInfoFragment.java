package walkinggb.android.knu.com.walkinggb.Main.MyInfo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import walkinggb.android.knu.com.walkinggb.DataService.UserInfo;
import walkinggb.android.knu.com.walkinggb.R;
import walkinggb.android.knu.com.walkinggb.Utils.ProcessData;

public class MyInfoFragment extends Fragment {

    static MyInfoFragment fragment;

    TextView nameView;
    TextView genderView;
    TextView ageView;
    TextView weightView;
    TextView levelView;
    TextView distanceView;
    TextView distanceView2;

    UserInfo userInfo;

    public static MyInfoFragment newInstance() {
        if (fragment == null) {
            fragment = new MyInfoFragment();
        }
        return fragment;
    }

    public MyInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_info, container, false);
        nameView = (TextView) rootView.findViewById(R.id.name);
        genderView = (TextView) rootView.findViewById(R.id.gender);
        ageView = (TextView) rootView.findViewById(R.id.age);
        weightView = (TextView) rootView.findViewById(R.id.weight);
        levelView = (TextView) rootView.findViewById(R.id.levelView);
        distanceView = (TextView) rootView.findViewById(R.id.distanceView);
        distanceView2 = (TextView) rootView.findViewById(R.id.distanceView2);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        String []levels = ProcessData.checkLevel(userInfo.getDistance());
        levelView.setText(levels[0]);
        distanceView.setText(levels[1]);
        distanceView2.setText(levels[2]);

        notifyDataChanged();
    }

    public void notifyDataChanged(){
        nameView.setText(userInfo.getName());
        genderView.setText(userInfo.getGender());
        ageView.setText(userInfo.getAge() + "");
        weightView.setText(userInfo.getWeight() + "");
    }

    public void setUserInfo(UserInfo userInfo){
        this.userInfo = userInfo;
    }
}
