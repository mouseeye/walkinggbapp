package walkinggb.android.knu.com.walkinggb.Main.MyRoute;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by Arron on 2015-08-23.
 */
public class MyRouteAdapter extends BaseAdapter {
    ArrayList<WalkingInfo> walkingInfos = new ArrayList<>();

    Context context;
    public MyRouteAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return walkingInfos.size();
    }

    @Override
    public Object getItem(int position) {
        return walkingInfos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyRouteView myRouteView;

        if(convertView == null){
            myRouteView = new MyRouteView(context);
        }else{
            myRouteView = (MyRouteView) convertView;
        }

        WalkingInfo walkingInfo = walkingInfos.get(position);

        myRouteView.setMyRouteView(walkingInfo);
        return myRouteView;
    }

    public void setWalkingInfos(ArrayList<WalkingInfo> walkingInfos){
        this.walkingInfos = walkingInfos;
    }
}
