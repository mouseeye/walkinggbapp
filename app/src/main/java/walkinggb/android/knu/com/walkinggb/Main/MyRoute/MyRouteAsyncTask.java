package walkinggb.android.knu.com.walkinggb.Main.MyRoute;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;

import walkinggb.android.knu.com.walkinggb.DataService.WalkingGBDatabase;
import walkinggb.android.knu.com.walkinggb.Main.MainActivity;
import walkinggb.android.knu.com.walkinggb.R;
import walkinggb.android.knu.com.walkinggb.Sub.SubItem;
import walkinggb.android.knu.com.walkinggb.Utils.CalcTime;

/**
 * Created by Arron on 2015-08-23.
 */
public class MyRouteAsyncTask extends AsyncTask<Object, Object, ArrayList<WalkingInfo>> {

    private Context context;
    private MyRouteAdapter myRouteAdapter;
    private ProgressDialog dialog;
    private WalkingGBDatabase db;

    public MyRouteAsyncTask(Context context, MyRouteAdapter myRouteAdapter){
        this.context = context;
        this.myRouteAdapter = myRouteAdapter;
    }


    @Override
    protected ArrayList<WalkingInfo> doInBackground(Object[] params) {
        db.open();
        String sql = "SELECT * FROM " + WalkingGBDatabase.walkingInfoTableName;
        Cursor cursor = db.rawQuery(sql, null);
        ArrayList<WalkingInfo> walkingInfos = initDataForAdapter(cursor);
        return walkingInfos;
    }

    @Override
    protected void onPreExecute() {
        db = WalkingGBDatabase.getDatabase(context);
        dialog = new ProgressDialog(context);
        dialog.setMessage("Data Loading....");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.show();
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(ArrayList<WalkingInfo> walkingInfos) {
        db.close();
        dialog.dismiss();

        if(walkingInfos.size() > 0){
            myRouteAdapter.setWalkingInfos(walkingInfos);
            myRouteAdapter.notifyDataSetChanged();
        }else{
            ((MainActivity)context).getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, NothingFragment.newInstance()).commit();
        }
    }

    private ArrayList<WalkingInfo> initDataForAdapter(Cursor cursor) {
        ArrayList<WalkingInfo> walkingInfos = new ArrayList<>();
        while (cursor.moveToNext()) {
            String region = cursor.getString(1);
            String main_street = cursor.getString(2);
            String sub_street = cursor.getString(3);
            String created_date = cursor.getString(4);
            String start_time = cursor.getString(5);
            String end_time = cursor.getString(6);
            String taken_time = cursor.getString(7);
            String distance = cursor.getString(8);
            String calorie = cursor.getString(9);
            String step_num = cursor.getString(10);
            WalkingInfo walkingInfo = new WalkingInfo();
            walkingInfo.setRegion(region);
            walkingInfo.setMainTitle(main_street);
            walkingInfo.setSubTitle(sub_street);
            walkingInfo.setCreatedDate(Long.parseLong(created_date));
            walkingInfo.setStartTime(Long.parseLong(start_time));
            walkingInfo.setEndTime(Long.parseLong(end_time));
            walkingInfo.setTakenTime(taken_time);
            walkingInfo.setDistance(Double.parseDouble(distance));
            walkingInfo.setCalorie(Double.parseDouble(calorie));
            walkingInfo.setStepNum(Integer.parseInt(step_num));
            walkingInfos.add(walkingInfo);
        }
        return walkingInfos;
    }
}
