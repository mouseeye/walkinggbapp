package walkinggb.android.knu.com.walkinggb.Main.MyRoute;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import walkinggb.android.knu.com.walkinggb.R;

public class MyRouteFragment extends Fragment {

    static MyRouteFragment fragment;
    ListView myRouteList;
    public static MyRouteFragment newInstance() {
        if(fragment == null){
            fragment = new MyRouteFragment();
        }
        return fragment;
    }

    public MyRouteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_route, container, false);
        myRouteList = (ListView) rootView.findViewById(R.id.myRouteList);

        myRouteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        loadRouteDate();
        return rootView;
    }

    private void loadRouteDate() {
        Context context = getActivity();
        MyRouteAdapter myRouteAdapter = new MyRouteAdapter(context);
        MyRouteAsyncTask myRouteAsyncTask = new MyRouteAsyncTask(context, myRouteAdapter);
        myRouteAsyncTask.execute();
        myRouteList.setAdapter(myRouteAdapter);
    }

}
