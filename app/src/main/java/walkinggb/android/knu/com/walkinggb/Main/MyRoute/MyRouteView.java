package walkinggb.android.knu.com.walkinggb.Main.MyRoute;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import walkinggb.android.knu.com.walkinggb.R;
import walkinggb.android.knu.com.walkinggb.Utils.CalcTime;

/**
 * Created by Arron on 2015-08-23.
 */
public class MyRouteView extends LinearLayout {
    TextView regionView;
    TextView mainStreet;
    TextView subStreet;
    TextView startTimeView;
    TextView takenTimeView;
    TextView endTimeView;
    TextView distanceView;
    TextView stepNumView;
    TextView calorieView;
    TextView createdDateView;
    public MyRouteView(Context context) {
        super(context);
        init(context);
    }

    public MyRouteView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.view_myroute, this, true);


        regionView = (TextView) findViewById(R.id.region);
        mainStreet = (TextView) findViewById(R.id.main_title);
        subStreet = (TextView) findViewById(R.id.sub_title);
        startTimeView = (TextView) rootView.findViewById(R.id.startTimeview);
        takenTimeView = (TextView) rootView.findViewById(R.id.takenTimeView);
        endTimeView = (TextView) rootView.findViewById(R.id.endTimeView);
        distanceView = (TextView) rootView.findViewById(R.id.distanceView);
        stepNumView = (TextView) rootView.findViewById(R.id.stepNumView);
        calorieView = (TextView) rootView.findViewById(R.id.calorieView);
        createdDateView = (TextView) rootView.findViewById(R.id.createdDateView);
    }

    public void setMyRouteView(WalkingInfo walkingInfo){

        mainStreet.setText(walkingInfo.getMainTitle());
        subStreet.setText(walkingInfo.getSubTitle());
        regionView.setText(walkingInfo.getRegion());
        startTimeView.setText(CalcTime.getUTCTime(walkingInfo.getStartTime()));
        takenTimeView.setText(walkingInfo.getTakenTime());
        endTimeView.setText(CalcTime.getUTCTime(walkingInfo.getEndTime()));
        distanceView.setText(walkingInfo.getDistance()+"m");
        stepNumView.setText(walkingInfo.getStepNum()+"걸음");
        calorieView.setText(walkingInfo.getCalorie()+"kcal");
        createdDateView.setText(CalcTime.getUTCDateTime(walkingInfo.getCreatedDate()));
    }
}
