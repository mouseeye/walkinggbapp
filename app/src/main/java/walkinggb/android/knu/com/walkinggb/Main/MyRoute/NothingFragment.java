package walkinggb.android.knu.com.walkinggb.Main.MyRoute;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import walkinggb.android.knu.com.walkinggb.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NothingFragment extends Fragment {

    private static NothingFragment nothingFragment;

    public static NothingFragment newInstance(){
        if (nothingFragment == null){
            nothingFragment = new NothingFragment();
        }
        return nothingFragment;
    }


    public NothingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_nothing, container, false);
        return rootView;
    }
}
