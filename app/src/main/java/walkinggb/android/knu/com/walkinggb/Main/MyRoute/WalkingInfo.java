package walkinggb.android.knu.com.walkinggb.Main.MyRoute;

import java.io.Serializable;

/**
 * Created by Arron on 2015-08-19.
 */
public class WalkingInfo implements Serializable{

    private String region;
    private String mainTitle;
    private String subTitle;
    private long createdDate;
    private long startTime;
    private long endTime;
    private String takenTime;
    private double distance;

    private double calorie;

    private int stepNum;

    public WalkingInfo(){};
    public WalkingInfo(String region, String mainTitle, String subTitle){
        this.region = region;
        this.subTitle = subTitle;
        this.mainTitle = mainTitle;
        this.distance = 0;
        this.calorie = 0;
    }
    public long getEndTime() {
        return endTime;
    }


    public void setCalorie(double calorie) {
        this.calorie = calorie;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public int getStepNum() {
        return stepNum;
    }

    public String getMainTitle() {
        return mainTitle;
    }

    public void setMainTitle(String mainTitle) {
        this.mainTitle = mainTitle;
    }
    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setStepNum(int stepNum) {
        this.stepNum = stepNum;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getStartTime(){
        return startTime;
    }

    public double getDistance(){
        return distance;
    }

    public double getCalorie(){
        return calorie;
    }

    public void setTakenTime(String takenTime) {
           this.takenTime = takenTime;
    }

    public void setDistance(double distance) {
        this.distance = this.distance + distance;
    }

    public void setEndTime(long endTime){
        this.endTime = endTime;
    }

    public String getTakenTime(){
        return takenTime;
    }
}
