package walkinggb.android.knu.com.walkinggb.MainMap;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;

import walkinggb.android.knu.com.walkinggb.DataService.Attraction;
import walkinggb.android.knu.com.walkinggb.Posting.PostingActivity;

/**
 * Created by Arron on 2015-08-23.
 */
public class AttractionProximityReceiver extends BroadcastReceiver {

    private String expectedAction;
    private Attraction attractions;

    public AttractionProximityReceiver(String expectedAction, Attraction attractions){
        this.attractions = attractions;
        this.expectedAction = expectedAction;
    }

    public IntentFilter getIntentFilter(){
        IntentFilter intentFilter = new IntentFilter(expectedAction);
        return intentFilter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        new AlertDialog.Builder(context)
                .setTitle("관광지 정보")
                .setMessage("근처에 " + "「문무왕릉」" + "이 있습니다. 과거 왜구의 침입을 막기 위해 용이 되고자 했던 문무대왕의 호국정신을 느껴보세요!")
                .setNegativeButton("확인", null)
                .show();
    }
}
