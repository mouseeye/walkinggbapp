package walkinggb.android.knu.com.walkinggb.MainMap;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import walkinggb.android.knu.com.walkinggb.Login.LoginActivity;
import walkinggb.android.knu.com.walkinggb.Main.MyRoute.WalkingInfo;
import walkinggb.android.knu.com.walkinggb.Posting.PostingActivity;
import walkinggb.android.knu.com.walkinggb.Utils.CalcTime;

public class EndProximityReceiver extends BroadcastReceiver {

    private ArrayList<LatLng> recordedLocation;
    private TextView endTimeView;
    private String expectedAction;
    private Handler handler;
    private WalkingInfo walkingInfo;

    public EndProximityReceiver(String expectedAction, TextView endTimeView, Handler handler, WalkingInfo walkingInfo, ArrayList<LatLng> recordedLocation) {
        this.expectedAction = expectedAction;
        this.endTimeView = endTimeView;
        this.handler = handler;
        this.walkingInfo = walkingInfo;
        this.recordedLocation = recordedLocation;
    }

    public IntentFilter getIntentFilter() {
        IntentFilter intentFilter = new IntentFilter(expectedAction);
        return intentFilter;
    }


    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.d("TAG", "EndProximity 호출~~");

        handler.post(new Runnable() {
            @Override
            public void run() {
                long endTime = System.currentTimeMillis();
                endTimeView.setText(CalcTime.getUTCTime(endTime));
                Log.d("TAG", CalcTime.getUTCTime(endTime));
            }
        });


        insertMyWalingInfoData(context);
        new AlertDialog.Builder(context)
                .setTitle("성공")
                .setMessage("축하합니다!!! "+"「" + walkingInfo.getSubTitle() + "」" + "의 마지막 지점에 도착하셨습니다.\n" +"페이스북 또는 카카오스토리로 친구들에게 알려보세요!")
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(context, LoginActivity.class);
                        intent.putExtra("walkingInfo", walkingInfo);
                        intent.putExtra("recordedLocation", recordedLocation);
                        context.startActivity(intent);
                    }
                })
                .setNegativeButton("취소", null)
                .show();
        MainMapActivity mainMapActivity = (MainMapActivity)context;
        mainMapActivity.endAllFunctions();
    }

    private void insertMyWalingInfoData(Context context) {
        walkingInfo.setCreatedDate(System.currentTimeMillis());
        walkingInfo.setEndTime(walkingInfo.getCreatedDate());
        InsertingMyWIAsyncTask insertingMyWIAsyncTask = new InsertingMyWIAsyncTask(context);
        insertingMyWIAsyncTask.execute(walkingInfo);
    }
}
