package walkinggb.android.knu.com.walkinggb.MainMap;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import walkinggb.android.knu.com.walkinggb.DataService.UserInfo;
import walkinggb.android.knu.com.walkinggb.DataService.WalkingGBDatabase;
import walkinggb.android.knu.com.walkinggb.Main.MyRoute.WalkingInfo;
import walkinggb.android.knu.com.walkinggb.Utils.CalcTime;
import walkinggb.android.knu.com.walkinggb.Utils.ProcessData;

/**
 * Created by Arron on 2015-08-22.
 */
public class InsertingMyWIAsyncTask extends AsyncTask<WalkingInfo, Object, Object> {

    private Context context;
    private WalkingGBDatabase walkingGBDatabase;
    private boolean isOpenedDatabase;
    private ProgressDialog dialog;

    public InsertingMyWIAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected Object doInBackground(WalkingInfo[] walkingInfo) {
        if (!isOpenedDatabase) {
            return null;
        }
        WalkingInfo wi = walkingInfo[0];
        String sql = "INSERT INTO " + WalkingGBDatabase.walkingInfoTableName + "(region, main_street, sub_street, created_date, start_time, end_time, taken_time, distance, calorie, step_num) values (" +
                "'" + wi.getRegion() + "'," +
                "'" + wi.getMainTitle() + "'," +
                "'" + wi.getSubTitle() + "'," +
                "'" + wi.getCreatedDate() + "'," +
                "'" + wi.getStartTime() + "'," +
                "'" + wi.getEndTime() + "'," +
                "'" + wi.getTakenTime() + "'," +
                "'" + wi.getDistance() + "'," +
                "'" + wi.getCalorie() + "'," +
                "'" + wi.getStepNum() + "')";
        walkingGBDatabase.execSQL(sql);

        UserInfo userInfo = ProcessData.loadUserInfo(context);
        String updateSQL = "UPDATE " + WalkingGBDatabase.userInfoTableName + " SET distance = " + "'" + wi.getDistance() + userInfo.getDistance() + "'" + " where _id =  1";

        walkingGBDatabase.execSQL(updateSQL);
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Data Saving...");
        dialog.show();

        walkingGBDatabase = WalkingGBDatabase.getDatabase(context);
        isOpenedDatabase = walkingGBDatabase.open();
    }

    @Override
    protected void onPostExecute(Object o) {
        walkingGBDatabase.close();
        dialog.dismiss();
        super.onPostExecute(o);
    }
}
