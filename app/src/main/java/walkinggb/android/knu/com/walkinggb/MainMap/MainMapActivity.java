package walkinggb.android.knu.com.walkinggb.MainMap;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

import walkinggb.android.knu.com.walkinggb.DataService.WalkingGBDatabase;
import walkinggb.android.knu.com.walkinggb.Main.MyRoute.WalkingInfo;
import walkinggb.android.knu.com.walkinggb.R;
import walkinggb.android.knu.com.walkinggb.Utils.CalcTime;
import walkinggb.android.knu.com.walkinggb.Utils.ProcessData;

/**
 * Created by Arron on 2015-07-31.
 */
public class MainMapActivity extends ActionBarActivity {
    GoogleMap googleMap;
    ArrayList<LatLng> latLngs;
    LocationManager locationManager;
    MyLocationListener locationListener;
    SupportMapFragment supportMapFragment;
    String region;
    String mainTitle;
    String subTitle;
    String latLon;
    int weight;
    LatLng attraction;
    PolylineOptions polylineOptions;

    Button startBtn;
    Button cancelBtn;
    TextView startTimeView;
    TextView calorieView;
    TextView distanceView;
    TextView endTimeView;
    TextView takenTimeView;
    TextView stepNumView;


    Handler handler = new Handler();
    EndProximityReceiver endProximityReceiver;
    AttractionProximityReceiver attractionProximityReceiver;

    boolean isRecording;
    ArrayList<LatLng> recordedLocation = new ArrayList<LatLng>();
    WalkingInfo walkingInfo;

    Intent manboServiceIntent;
    ManboResultReceiver manboResultReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_map);

        //액션바 초기 세팅
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        region = intent.getStringExtra("region");
        mainTitle = intent.getStringExtra("mainTitle");
        subTitle = intent.getStringExtra("subTitle");
        latLon = intent.getStringExtra("latLon");
        actionBar.setTitle(subTitle);

        //구글맵 초기화
        supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        googleMap = supportMapFragment.getMap();
        googleMap.setMyLocationEnabled(true);
        latLngs = ProcessData.requestRouteData(latLon);


        //locationManager 초기화
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        isRecording = false;

        //ManboService 초기화
        manboServiceIntent = new Intent(this, ManboCountService.class);
       // manboResultReceiver = new ManboResultReceiver();

        //view 초기화
        startBtn = (Button) findViewById(R.id.startBtn);
        cancelBtn = (Button) findViewById(R.id.cancelBtn);
        startTimeView = (TextView) findViewById(R.id.startTimeview);
        distanceView = (TextView) findViewById(R.id.distanceView);
        calorieView = (TextView) findViewById(R.id.calorieView);
        takenTimeView = (TextView) findViewById(R.id.takenTimeView);
        endTimeView = (TextView) findViewById(R.id.endTimeView);
        stepNumView = (TextView) findViewById(R.id.stepNumView);
    }

    public void onStartBtnClicked(View v) {
        if (!checkGPSEnabled()) {
            return;
        }

        drawFollowingRoute();

        //메인맵 화면 정보 업데이트 초기화
        long time = System.currentTimeMillis();
        walkingInfo = new WalkingInfo(region, mainTitle, subTitle);
        walkingInfo.setStartTime(time);
        startTimeView.setText(CalcTime.getUTCTime(time));
        distanceView.setText(walkingInfo.getDistance() + "m");
        calorieView.setText(walkingInfo.getCalorie() + "kcal");
        getWeightFromDB();

        isRecording = true;
        startBtn.setText("Recording..");
        startBtn.setTextColor(Color.GRAY);
        startBtn.setClickable(false);

        cancelBtn.setTextColor(Color.parseColor("#1E88E5"));
        cancelBtn.setClickable(true);

        startLocationUpdate();
        manboStart();
        registerEndLocation();
        registerAttractionLocation();
    }

    private void manboStart() {
        manboResultReceiver = new ManboResultReceiver();
        IntentFilter intentFilter = new IntentFilter("collectStepNum");
        registerReceiver(manboResultReceiver, intentFilter);

        startService(manboServiceIntent);
    }

    public void onCancelBtnClicked(View v) {

        walkingInfo = null;
        isRecording = false;

        if(endProximityReceiver != null && locationListener != null){
            unregisterReceiver(endProximityReceiver);
            unregisterReceiver(attractionProximityReceiver);
            endProximityReceiver = null;
            endLocationUpdate();
        }

        manboEnd();
        recordedLocation.clear();
        googleMap.clear();
        setViewDefault();

        cancelBtn.setTextColor(Color.GRAY);
        cancelBtn.setClickable(false);

        startBtn.setText("Start");
        startBtn.setTextColor(Color.parseColor("#1E88E5"));
        startBtn.setClickable(true);

    }

    public void endAllFunctions(){
        unregisterReceiver(endProximityReceiver);
        unregisterReceiver(attractionProximityReceiver);
        endProximityReceiver = null;
        endLocationUpdate();
    }

    private void setViewDefault(){
        startTimeView.setText("--시 --분");
        takenTimeView.setText("--시간 --분");
        endTimeView.setText("--시 --분");
        distanceView.setText("--m");
        calorieView.setText("---kcal");
        stepNumView.setText("-걸음");
    }

    private boolean checkGPSEnabled() {
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!isGPSEnabled) {
            new AlertDialog.Builder(this)
                    .setTitle("GPS 설정 요청")
                    .setMessage("위치 정보 확인 옵션이 설정되어 있지 않습니다. 「걸어서 경상북도」를 이용하기 위해서는 '위치 및 보안 설정'의 무선 네트워크 및 GPS 위성 옵션에 대해 '사용'으로 설정 하셔야 해용")
                    .setPositiveButton("설정", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("취소", null)
                    .show();
        }
        return isGPSEnabled;
    }

    private void startLocationUpdate() {
        locationListener = new MyLocationListener();
        long minTime = 1000;
        float minDistance = 1F;

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDistance, locationListener);

        //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, minTime, minDistance, locationListener);
    }

    public void endLocationUpdate(){
        locationManager.removeUpdates(locationListener);
    }

    private void manboEnd(){
        unregisterReceiver(manboResultReceiver);
        stopService(manboServiceIntent);
    }

    private void drawFollowingRoute(){

        MarkerOptions startMarker = new MarkerOptions();
        startMarker.title("출발점")
                .position(latLngs.get(0));

        MarkerOptions endMarker = new MarkerOptions();
        endMarker.title("도착점")
                    .position(latLngs.get(latLngs.size() - 1));

        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.addAll(latLngs);
        polylineOptions.color(Color.parseColor("#44FF7923"));
        googleMap.addPolyline(polylineOptions);
        googleMap.addMarker(startMarker);
        googleMap.addMarker(endMarker);
    }

    private void registerEndLocation() {
        LatLng end = latLngs.get(latLngs.size()-1);

        String endAction = "endProximity";
        Intent endIntent = new Intent(endAction);
        int endID = 1002;
        endIntent.putExtra("id", endID);
        endIntent.putExtra("end", end);
        PendingIntent endPendingIntent = PendingIntent.getBroadcast(this, endID, endIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        locationManager.addProximityAlert(end.latitude, end.longitude, 100, -1, endPendingIntent);
        //locationManager.addProximityAlert(35.8515604, 128.5256918, 1000, -1, endPendingIntent);//집
        //locationManager.addProximityAlert(35.853638, 128.545767, 100, -1, endPendingIntent);//35.853638, 128.545767  서남시장
        locationManager.addProximityAlert(35.853000, 128.529597, 50, -1, endPendingIntent);//학생
        //locationManager.addProximityAlert(35.850905, 128.524819, 15, -1, endPendingIntent);//편의점
        //locationManager.addProximityAlert(35.850905, 128.524819, 20, -1, endPendingIntent);//경로따라 걷는길
        endProximityReceiver = new EndProximityReceiver(endAction, endTimeView, handler, walkingInfo, recordedLocation);
        IntentFilter intentFilter = endProximityReceiver.getIntentFilter();
        registerReceiver(endProximityReceiver, intentFilter);
    }

    private void registerAttractionLocation(){
        String attractionAction = "attractionProximity";
        Intent attractionIntent = new Intent(attractionAction);
        int attractionID = 1003;
        attraction = new LatLng(35.738034, 129.484826);
        attractionIntent.putExtra("id", attractionID);
        attractionIntent.putExtra("attraction", attraction);
        PendingIntent attractionPendingIntent = PendingIntent.getBroadcast(this, attractionID, attractionIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        locationManager.addProximityAlert(35.738034, 129.484826, 100, -1, attractionPendingIntent);
         attractionProximityReceiver = new AttractionProximityReceiver(attractionAction, null);
        IntentFilter intentFilter = attractionProximityReceiver.getIntentFilter();
        registerReceiver(attractionProximityReceiver, intentFilter);
    }

    private void showCurrentLocation(Double latitude, Double longitude) {

        LatLng latLng = new LatLng(latitude, longitude);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

    private void recordCurrentLocation(Double latitude, Double longitude) {
        LatLng latLng = new LatLng(latitude, longitude);
        recordedLocation.add(latLng);

        drawMyRoute(latLng);

        updateWalkingInfo(recordedLocation);
        Log.d("TAG", "recording.." + latLng.latitude);
    }

    private void drawMyRoute(LatLng location) {
        if(recordedLocation.get(0) != null){
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.title("MY시작점");
            markerOptions.position(recordedLocation.get(0));
            googleMap.addMarker(markerOptions);
        }

        polylineOptions = new PolylineOptions();
        polylineOptions.color(Color.GREEN);
        polylineOptions.addAll(recordedLocation);
        googleMap.addPolyline(polylineOptions);

        drawFollowingRoute();
    }

    private void updateWalkingInfo(ArrayList<LatLng> recordedLocation){
        String takenTime = CalcTime.getTakenTime(System.currentTimeMillis(), walkingInfo.getStartTime());
        double distance;

        walkingInfo.setTakenTime(takenTime);
        takenTimeView.setText(takenTime);

        if(recordedLocation.size()>1){
            distance = CalcTime.getDistanceBetween(recordedLocation.get(recordedLocation.size()-1), recordedLocation.get(recordedLocation.size()-2));
        }else{
            distance = 0.0;
        }
        walkingInfo.setDistance(distance);
        distanceView.setText((int) walkingInfo.getDistance() + "m");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:
                if(endProximityReceiver != null && locationListener != null){
                    unregisterReceiver(endProximityReceiver);
                    unregisterReceiver(attractionProximityReceiver);
                    endLocationUpdate();
                }
                recordedLocation.clear();

                finish();
                overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(endProximityReceiver != null && locationListener != null){
            unregisterReceiver(endProximityReceiver);
            unregisterReceiver(attractionProximityReceiver);
            endLocationUpdate();
        }

        recordedLocation.clear();

        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }

    public void getWeightFromDB() {
        WalkingGBDatabase db = WalkingGBDatabase.getDatabase(this);
        db.open();
        String sql = "select * from " + WalkingGBDatabase.userInfoTableName + " where _id  = 1";
        Cursor cursor = db.rawQuery(sql, null);
        if(cursor.moveToNext()){
            weight = cursor.getInt(1);
        }
    }

    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            Double latitude = location.getLatitude();
            Double longitude = location.getLongitude();

            showCurrentLocation(latitude, longitude);

            if (isRecording) {
                recordCurrentLocation(latitude, longitude);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {
            //showCurrentLocation(latitude, longitude);
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    }


    class ManboResultReceiver extends BroadcastReceiver {
        int stepNum;
        double calorie;
        @Override
        public void onReceive(Context context, Intent intent) {
            stepNum = intent.getIntExtra("stepNum", 0);
            stepNumView.setText(stepNum + "걸음");
            walkingInfo.setStepNum(stepNum);
            calorie = CalcTime.getCalorie(weight, stepNum);
            calorieView.setText(calorie+"kcal");
            walkingInfo.setCalorie(calorie);
        }
    }
}

