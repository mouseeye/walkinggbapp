package walkinggb.android.knu.com.walkinggb.Posting;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import walkinggb.android.knu.com.walkinggb.Main.MyRoute.WalkingInfo;
import walkinggb.android.knu.com.walkinggb.R;
import walkinggb.android.knu.com.walkinggb.Utils.CalcTime;

public class PostingActivity extends AppCompatActivity {

    TextView regionView;
    TextView mainStreet;
    TextView startTimeView;
    TextView subStreet;

    TextView endTimeView;
    TextView takenTimeView;
    TextView distanceView;
    TextView calorieView;
    GoogleMap googleMap;
    ViewGroup postingView;
    TextView stepNumView;

    ArrayList<LatLng> recordedLocation;
    WalkingInfo walkingInfo;
    Bitmap mapImage;
    CallbackManager callbackManager;
    ShareDialog shareDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_posting);

        regionView = (TextView) findViewById(R.id.region);
        mainStreet = (TextView) findViewById(R.id.main_title);
        subStreet = (TextView) findViewById(R.id.sub_title);
        startTimeView = (TextView) findViewById(R.id.startTimeview);
        endTimeView = (TextView) findViewById(R.id.endTimeView);
        takenTimeView = (TextView) findViewById(R.id.takenTimeView);
        distanceView = (TextView) findViewById(R.id.distanceView);
        calorieView = (TextView) findViewById(R.id.calorieView);
        postingView = (ViewGroup) findViewById(R.id.postingView);
        stepNumView = (TextView) findViewById(R.id.stepNumView);

        googleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

        Intent intent = getIntent();
        recordedLocation = intent.getParcelableArrayListExtra("recordedLocation");
        walkingInfo = (WalkingInfo) intent.getSerializableExtra("walkingInfo");


        setWalkingInfoView();
        setMapRoute();
    }

    public void setWalkingInfoView() {
        regionView.setText(walkingInfo.getRegion());
        mainStreet.setText(walkingInfo.getMainTitle());
        subStreet.setText(walkingInfo.getSubTitle());
        startTimeView.setText(CalcTime.getUTCTime(walkingInfo.getStartTime()));
        endTimeView.setText(CalcTime.getUTCTime(walkingInfo.getEndTime()));
        takenTimeView.setText(walkingInfo.getTakenTime());
        distanceView.setText(walkingInfo.getDistance() + "m");
        stepNumView.setText(walkingInfo.getStepNum() + "걸음");
        calorieView.setText(walkingInfo.getCalorie() + "kcal");
    }

    public void setMapRoute() {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (recordedLocation != null && recordedLocation.size() > 0) {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(recordedLocation.get(recordedLocation.size() / 2), 16));

            MarkerOptions startMarker = new MarkerOptions();
            startMarker.position(recordedLocation.get(0));
            startMarker.title("시작점");

            MarkerOptions endMarker = new MarkerOptions();
            if (recordedLocation.size() > 0) {
                endMarker.position(recordedLocation.get(recordedLocation.size() - 1));
                endMarker.title("도착점");
            }

            PolylineOptions polylineOptions = new PolylineOptions();
            polylineOptions.addAll(recordedLocation);
            polylineOptions.color(Color.GREEN);
            googleMap.addPolyline(polylineOptions);
            googleMap.addMarker(endMarker);
            googleMap.addMarker(startMarker);
        }
    }

    public void onUploadBtnClicked(View v) {
        Log.d("TAG", "UploadBtn 호출");
        shareDialog = new ShareDialog(PostingActivity.this);
        postingView.buildDrawingCache();
        Bitmap image = postingView.getDrawingCache();
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(Environment.getExternalStorageDirectory().toString() + "/capture.jpeg");
            image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        googleMapCapture();

        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .build();

        SharePhoto mapPhoto = new SharePhoto.Builder()
                .setBitmap(mapImage)
                .build();

        ArrayList<SharePhoto> sharePhotos = new ArrayList<SharePhoto>();
        sharePhotos.add(photo);
        sharePhotos.add(mapPhoto);

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhotos(sharePhotos)
                .build();

        ShareDialog.show(PostingActivity.this, content);
        Log.d("TAG", "Uploadshow 호출");
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.d("TAG", "공유 성공");
            }

            @Override
            public void onCancel() {
                Log.d("TAG", "공유 실패");
            }

            @Override
            public void onError(FacebookException e) {
                Log.d("TAG", "공유 실패 : " + e);
            }
        });
    }

    public void googleMapCapture() {

        googleMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
            @Override
            public void onSnapshotReady(Bitmap bitmap) {
                FileOutputStream fos = null;
                if (bitmap == null) {
                    Toast.makeText(getApplicationContext(), "null", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        fos = new FileOutputStream(Environment.getExternalStorageDirectory().toString() + "/capture.jpeg");
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            fos.close();
                            mapImage = bitmap;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }
    public void onCancelBtnClicked(View v) {
        this.finish();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}