package walkinggb.android.knu.com.walkinggb.Sub;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;


import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import walkinggb.android.knu.com.walkinggb.DataService.WalkingGBDatabase;
import walkinggb.android.knu.com.walkinggb.Utils.ProcessData;

/**
 * Created by Arron on 2015-07-25.
 */
public class SettingSubItemAsyncTask extends AsyncTask {

    private SubItemAdapter adapter;
    private String mainStreet;
    private Context context;


    ProgressDialog dialog;
    private WalkingGBDatabase db;


    public SettingSubItemAsyncTask(Context context, SubItemAdapter adapter, String mainStreet) {
        this.context = context;
        this.adapter = adapter;
        this.mainStreet = mainStreet;
    }


    @Override
    protected void onPreExecute() {
        db = WalkingGBDatabase.getDatabase(context);
        dialog = new ProgressDialog(context);
        dialog.setMessage("Image Loading....");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.show();
        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(Object[] params) {
        db.open();
        String sql = "SELECT sub_street, image_url FROM " + WalkingGBDatabase.streetTableName + " WHERE main_street = ?";
        Cursor cursor = db.rawQuery(sql, new String[]{mainStreet});
        ArrayList<SubItem> subItems = initDataForAdapter(cursor);
        return subItems;
    }

    @Override
    protected void onPostExecute(Object subItems) {
        db.close();
        dialog.dismiss();
        adapter.setSubItems((ArrayList) subItems);
        adapter.notifyDataSetChanged();
    }


    private ArrayList<SubItem> initDataForAdapter(Cursor cursor) {
        ArrayList<SubItem> subItems = new ArrayList<>();
        Bitmap bitmap = null;
        while (cursor.moveToNext()) {
            String sub_street = cursor.getString(0);
            String image_url = cursor.getString(1);
            bitmap = ProcessData.loadImageFromServer(image_url);

            SubItem subItem = new SubItem();
            subItem.setSubTitle(sub_street);
            subItem.setSubImageBitmap(bitmap);

            subItems.add(subItem);
        }
        return subItems;
    }


}
