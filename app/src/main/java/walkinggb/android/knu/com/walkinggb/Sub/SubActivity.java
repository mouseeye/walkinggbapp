package walkinggb.android.knu.com.walkinggb.Sub;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;


import walkinggb.android.knu.com.walkinggb.Detail.DetailActivity;
import walkinggb.android.knu.com.walkinggb.R;


public class SubActivity extends ActionBarActivity {


    private GridView gridView;
    private SubItemAdapter subItemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);
        gridView = (GridView) findViewById(R.id.gridView);
        gridView.setNumColumns(2);
        subItemAdapter = new SubItemAdapter(this);

        Intent intent = getIntent();
        String main_street = intent.getStringExtra("main_street");
        loadSubItem(main_street);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(main_street);
        actionBar.setDisplayHomeAsUpEnabled(true);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SubItem subItem = (SubItem) subItemAdapter.getItem(position);
                String subStreet = subItem.getSubTitle();
                Intent i = new Intent(getApplicationContext(), DetailActivity.class);
                i.putExtra("sub_street", subStreet);
                startActivity(i);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        });
    }

    private void loadSubItem(String mainStreet) {
        SettingSubItemAsyncTask subItemAsyncTask = new SettingSubItemAsyncTask(this, subItemAdapter, mainStreet);
        subItemAsyncTask.execute();
        gridView.setAdapter(subItemAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch(id){
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }
}
