package walkinggb.android.knu.com.walkinggb.Sub;

import android.graphics.Bitmap;

/**
 * Created by Arron on 2015-07-19.
 */
public class SubItem {
    private String subTitle;
    private Bitmap subImageBitmap;

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public Bitmap getSubImageBitmap() {
        return subImageBitmap;
    }

    public void setSubImageBitmap(Bitmap subImageBitmap) {
        this.subImageBitmap = subImageBitmap;
    }
}
