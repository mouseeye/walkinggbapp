package walkinggb.android.knu.com.walkinggb.Sub;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by Arron on 2015-07-19.
 */
public class SubItemAdapter extends BaseAdapter {

    private Context context;
    ArrayList<SubItem> subItems = new ArrayList<>();

    public SubItemAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return subItems.size();
    }

    @Override
    public Object getItem(int position) {
        return subItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SubItemView subItemView;

        if(convertView == null){
            subItemView = new SubItemView(context);
        }else{
            subItemView = (SubItemView) convertView;
        }
        SubItem item = subItems.get(position);
        subItemView.setSubImageBitmap(item.getSubImageBitmap());
        subItemView.setSubTitle("# "+item.getSubTitle());

        return subItemView;
    }

    public void setSubItems(ArrayList subItems){
        if(subItems !=null){
            this.subItems = subItems;
        }
    }
}
