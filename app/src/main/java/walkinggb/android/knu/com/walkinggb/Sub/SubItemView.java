package walkinggb.android.knu.com.walkinggb.Sub;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import walkinggb.android.knu.com.walkinggb.R;

/**
 * Created by Arron on 2015-07-19.
 */
public class SubItemView extends RelativeLayout {

    private Context context;
    private TextView subTitleView;
    private ImageView subImageView;
    public SubItemView(Context context) {
        super(context);
        init(context);
    }

    public SubItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context){
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_item_sub, this, true);
        subTitleView = (TextView) view.findViewById(R.id.subTitle);
        subImageView = (ImageView) view.findViewById(R.id.backgroundImage);
    }

    public void setSubTitle(String subTitle) {
        if(subTitle !=null){
            subTitleView.setText(subTitle);
        }
    }

    public void setSubImageBitmap(Bitmap subImageBitmap) {
        if(subImageBitmap != null){
            subImageView.setImageBitmap(subImageBitmap);
        }
    }
}
