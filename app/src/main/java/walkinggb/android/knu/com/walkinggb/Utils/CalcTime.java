package walkinggb.android.knu.com.walkinggb.Utils;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Arron on 2015-08-19.
 */
public class CalcTime {
    public static String getUTCTime(long sec){
        String time;
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH시 mm분");
        time = timeFormat.format(new Date(sec));
        return time;
    }

    public static String getUTCDateTime(long sec){
        String date;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy년 MM월 dd일 HH시 mm분");
        date = dateFormat.format(new Date(sec));
        return date;
    }

    public static String getTakenTime(long endTime, long startTime){
        long takenTime = (endTime-startTime)/1000;

        String hour = takenTime/3600 + "시간";
        String minute = takenTime%3600/60 + "분";

        return hour + " " + minute;
    }

    public static double getDistanceBetween(LatLng endDistance, LatLng startDistance){
        float results[] = {0, 0, 0};
        Location.distanceBetween(startDistance.latitude, startDistance.longitude, endDistance.latitude, endDistance.longitude, results);
        Log.d("TAG", "raw distance : " + results[0]);
        return Math.round(results[0]);
    }

    public static double getCalorie(int weight ,  int stepNum){
            return Math.round((weight * stepNum /2000.0)*100)/100.0;
    }
}