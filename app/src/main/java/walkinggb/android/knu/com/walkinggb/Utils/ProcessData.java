package walkinggb.android.knu.com.walkinggb.Utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.android.gms.maps.model.LatLng;

import java.io.BufferedReader;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.StringTokenizer;

import walkinggb.android.knu.com.walkinggb.DataService.UserInfo;
import walkinggb.android.knu.com.walkinggb.DataService.WalkingGBDatabase;

/**
 * Created by Arron on 2015-08-19.
 */
public class ProcessData {
    public static ArrayList<LatLng> requestRouteData(String latLon) {
        String longitude = null;
        String latitude = null;
        ArrayList<LatLng> latLngs = new ArrayList<>();
        StringTokenizer stringTokenizer = new StringTokenizer(latLon, ", ");

        while (stringTokenizer.hasMoreTokens()) {
            longitude = stringTokenizer.nextToken();
            latitude = stringTokenizer.nextToken();
            latLngs.add(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)));
        }
        return latLngs;
    }

    public static Bitmap loadImageFromServer(String image_url) {
        Bitmap bitmap = null;
        try {
            URL url = new URL(image_url);
            InputStream inputStream = url.openStream();
            bitmap = BitmapFactory.decodeStream(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static UserInfo loadUserInfo(Context context){
        String sql = "SELECT name, gender, age, weight, distance FROM " + WalkingGBDatabase.userInfoTableName;
        WalkingGBDatabase database = WalkingGBDatabase.getDatabase(context);
        database.open();
        Cursor cursor = database.rawQuery(sql, new String[]{});
        cursor.moveToNext();
        String name = cursor.getString(0);
        String gender = cursor.getString(1);
        int age = cursor.getInt(2);
        int weight = cursor.getInt(3);
        int distance = cursor.getInt(4);
        UserInfo userInfo = new UserInfo();
        userInfo.setName(name);
        userInfo.setGender(gender);
        userInfo.setAge(age);
        userInfo.setWeight(weight);
        userInfo.setDistance(distance);
        return userInfo;
    }

    public static String[] checkLevel(int distance){
        String[] levles = null;
        if(distance >=0 && distance < 10){
            levles = new String[]{Level.LEVEL[0], distance+"km 걸으셨습니다.", "다음 단계는 초보단계(10km ~ ) 입니다."};
            return  levles;
        }else if(distance >=10 && distance < 30){
            levles = new String[]{Level.LEVEL[1], distance+"km 걸으셨습니다.", "다음 단계는 중수단계(10km ~ ) 입니다."};
            return levles;
        }else if(distance >= 30 && distance < 60){
            levles = new String[]{Level.LEVEL[2], distance+"km 걸으셨습니다.", "다음 단계는 고수단계(30km ~ ) 입니다."};
            return levles;
        }else if(distance >= 60 && distance < 200){
            levles = new String[]{Level.LEVEL[3], distance+"km 걸으셨습니다.", "다음 단계는 고수단계(60km ~ ) 입니다."};
            return levles;
        }else if(distance >= 200){
            levles = new String[]{Level.LEVEL[4], distance+"km 걸으셨습니다.", "다음 단계는 초고수단계(200km ~ ) 입니다."};
            return levles;
        }else {
            levles = new String[]{"데이터 오류", distance+"잘못된 이동거리 정보가 저장 되어 있습니다.",""};
            return levles;
        }
    }
}
